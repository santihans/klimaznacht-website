import axios from 'axios'
import { pathOr } from 'ramda'

export default async () => {
  const url = `${process.env.baseUrl}/api/collections/get/klimaznacht`
  const response = await axios.get(url)
  return pathOr([], ['data', 'entries'])(response)
}
